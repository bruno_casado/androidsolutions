package com.desafioconcrete.android.util;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;

public class JsonSharedPreferences {

	private static final String PREFIX = "json";
	
	public static void saveJSONArray(Context c, String prefName, String key, JSONArray array){
		SharedPreferences settings = c.getSharedPreferences(prefName, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PREFIX+key, array.toString());
		editor.commit();
	}
	
	public static JSONArray loadJSONArray(Context c, String prefName, String key) throws JSONException{
		SharedPreferences settings = c.getSharedPreferences(prefName, 0);
		return new JSONArray(settings.getString(PREFIX+key, "[]"));
	}
}

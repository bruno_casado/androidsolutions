package com.desafioconcrete.android.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.desafioconcrete.android.Model.Endereco;
import com.google.gson.Gson;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class ExibirConsultasDialogFragment extends DialogFragment{
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState){
		Endereco endereco;
		String saida = "";
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			JSONArray teste = JsonSharedPreferences.loadJSONArray(getActivity(), "Enderecos", "cep");
			int tam = teste.length();
			if (tam > 0){
				for (int i = 0; i < tam; i++){
					JSONObject object = teste.getJSONObject(i);
					Gson gson = new Gson();
					Log.e("JSON", object.toString());
					endereco = gson.fromJson(object.toString(), Endereco.class);
					saida += endereco.getTipo() + " " + endereco.getLogradouro() + ".\n" + "Bairro: " + endereco.getBairro()
							+".\n" + endereco.getCidade() + " / " + endereco.getEstado() + "\nCEP: " + endereco.getCep() + "\n" 
							+ "----------------------------------------------------\n" ;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		builder.setMessage(saida);
		builder.setTitle("Consultas Anteriores");
		builder.setNeutralButton("Fechar", new DialogInterface.OnClickListener() {	
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub	
			}
		});
		
		AlertDialog dialog = builder.create();
		return dialog;
	}
}
